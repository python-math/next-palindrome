def next_palindrome(num):
    while True :
        if num > 10 and num == int(str(num)[::-1]):
            break
        else:
            num += 1
    return num
test_c = int(input('Enter how many times you want to run : '))
for i in range(test_c):
    num = int(input('Enter your number : '))
    print(f'Next palindrome of {num} is {next_palindrome(num)}')
